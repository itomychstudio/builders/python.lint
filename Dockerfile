FROM python:3.11-alpine


RUN pip3 install --no-cache-dir --upgrade\
        pycodestyle\
        yamllint\
        pylint\
        virtualenv\
    && rm -rf\ 
        /tmp/*\
        /var/cache/apk/*


